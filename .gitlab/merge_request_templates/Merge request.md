<!--
Thank you for your participation!

Please, fill the sections or remove if unnecessary.
Note that this formalism only helps us to manage the MRs easily.
-->

<!-- Describe your merge request here -->

## Checks

- [ ] I created/updated RSpec tests
- [ ] I created/updated Cucumber scenarios
- [ ] I created/updated acceptance tests <!-- Only keep this if you created/updated a controller -->
- [ ] I created/updated FactoryBot factories (preferably using Faker)
- [ ] My commits follows the [conventional commits format](https://www.conventionalcommits.org)
- [ ] I updated the readme to reflect my changes
- [ ] I updated CHANGELOG.md, including my work in the "Next" section

