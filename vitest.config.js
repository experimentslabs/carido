import { defineConfig } from 'vitest/config'
import RubyPlugin from 'vite-plugin-ruby'

export default defineConfig({
  plugins: [RubyPlugin()],
  test: {
    include: [
      '**/*.spec.js',
    ],
    environment: 'jsdom',
  },
})
