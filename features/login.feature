Feature: Login
  As an active user
  In order to use the service
  I want to be able to login

  Background:
    Given I have an account
    And I'm not logged in
    And I access the service

  Scenario: Login with valid password
    When I fill and submit the login form with my credentials
    Then I see my dashboard
