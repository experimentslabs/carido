require 'simplecov'
require 'capybara-screenshot/cucumber'
require 'selenium-webdriver'
require 'webdrivers/geckodriver'

Capybara.register_driver(:selenium) do |app|
  options         = Selenium::WebDriver::Firefox::Options.new
  firefox_profile = Selenium::WebDriver::Firefox::Profile.new

  # Custom Firefox profile to use light theme.
  firefox_profile['ui.systemUsesDarkTheme'] = 0

  args = []
  args << '--headless' unless ENV.fetch('HEADLESS', 'true') == 'false'

  options.profile = firefox_profile
  options.args = args

  Capybara::Selenium::Driver.new app,
                                 browser: :firefox,
                                 options: options
end

Capybara.default_driver    = :selenium
Capybara.javascript_driver = :selenium

Capybara::Screenshot.prune_strategy = :keep_last_run

Capybara.save_path = Rails.root.join('tmp', 'capybara_screenshots')
