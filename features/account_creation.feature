Feature: Account creation
  As a visitor,
  In order to use the service,
  I want to create an account

  Background:
    Given I'm a visitor
    And I access the service

  Scenario: Account creation with valid parameters
    Given I see the login form
    And I click on "Create account"
    And I fill and submit the account creation form with valid data
    Then I see a message stating that an email has been sent

  Scenario: Account creation with invalid parameters
    Given I see the login form
    And I click on "Create account"
    And I fill and submit the account creation form with invalid email
    Then I see a message stating that the email is invalid
