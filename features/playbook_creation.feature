Feature: Playbook creations
  As an user of the service,
  In order to prepare tasks
  I want to create playbooks

  Background:
    Given I'm already logged in
    And I access the service

  Scenario: Playbook creation
    Given I click on "New playbook"
    When I create the "Yakitori" playbook
    Then I see the "Yakitori playbook" in the playbooks list

  Scenario: Display a playbook
    Given I have a "Deploy new instance" playbook
    When I display the "Deploy new instance" playbook
    Then I see the details of the playbook

  Scenario: Add tasks to playbook
    Given I have a "Deploy new instance" playbook
    When I display the "Deploy new instance" playbook
    And I click on "Add task"
    And I fill and submit the task form in the modal
    Then I see the new task in the task list

  Scenario: Reorder tasks
    Given I have a "Deploy new instance" playbook with two tasks
    When I display the "Deploy new instance" playbook
    And I drag and drop the second task on the first position
    Then the second task is now the first one

  Scenario: Edit a playbook
    Given I have a "Deploy new instance" playbook
    When I display the "Deploy new instance" playbook
    And I click on "Edit"
    And I rename the playbook in the modal
    Then the playbook name is changed

  Scenario: Reorder tasks
    Given I have a "Deploy new instance" playbook with two tasks
    When I display the "Deploy new instance" playbook
    And I click on "Edit" on the first task
    And I rename the task in the modal
    Then the first task name is changed
