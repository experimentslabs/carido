Feature: Account activation
  As a fresh new user
  In order to use the service
  I want to activate my account

  Background:
    Given I created an account without activating it

  Scenario: Activating account
    When I access the page from the account activation link received in email
    Then I see a message stating that my account is now active
    And I see my dashboard
