require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe '/plays' do
  # This should return the minimal set of attributes required to create a valid
  # Play. As you add validations to Play, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) do
    FactoryBot.build(:play).attributes
  end

  let(:invalid_attributes) do
    { title: '' }
  end

  let(:playbook) { FactoryBot.create :playbook, user: user }

  include_context 'with an authenticated user'

  describe 'GET /index' do
    it 'renders a successful response' do
      FactoryBot.create_list :play, 3, playbook: playbook
      get playbook_plays_url(playbook_id: playbook.id)
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      play = FactoryBot.create :play, playbook: playbook
      get playbook_play_url(play, playbook_id: playbook.id)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_playbook_play_url(playbook_id: playbook.id)
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'renders a successful response' do
      play = FactoryBot.create :play, playbook: playbook
      get edit_playbook_play_url(play, playbook_id: playbook.id)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Play' do
        expect do
          post playbook_plays_url(playbook_id: playbook.id), params: { play: valid_attributes }
        end.to change(Play, :count).by(1)
      end

      it 'redirects to the play' do
        post playbook_plays_url(playbook_id: playbook.id), params: { play: valid_attributes }
        expect(response).to redirect_to(playbook_play_url(Play.last, playbook_id: playbook.id))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Play' do
        expect do
          post playbook_plays_url(playbook_id: playbook.id), params: { play: invalid_attributes }
        end.not_to change(Play, :count)
      end

      it "renders a response with 422 status (i.e. to display the 'new' template)" do
        post playbook_plays_url(playbook_id: playbook.id), params: { play: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) do
        { title: 'New title' }
      end

      it 'updates the requested play' do
        play = FactoryBot.create :play, playbook: playbook
        patch play_url(play), params: { play: new_attributes }
        play.reload
        expect(play.title).to eq 'New title'
      end

      it 'redirects to the play' do
        play = FactoryBot.create :play, playbook: playbook
        patch play_url(play), params: { play: new_attributes }
        play.reload
        expect(response).to redirect_to(playbook_play_url(play, playbook_id: playbook.id))
      end
    end

    context 'with invalid parameters' do
      it "renders a response with 422 status (i.e. to display the 'edit' template)" do
        play = FactoryBot.create :play, playbook: playbook
        patch play_url(play), params: { play: invalid_attributes }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe '/next_step' do
    it 'goes to the next step' do
      play = FactoryBot.create :play, playbook: playbook
      put next_step_play_url(play)
      play.reload

      expect(play.current_step_id).to eq play.steps_data.second['id']
    end
  end

  describe '/finish' do
    it 'finishes the play' do
      play = FactoryBot.create :play, playbook: playbook
      put finish_play_url(play)
      play.reload

      expect(play).to be_considered_done
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested play' do
      play = FactoryBot.create :play, playbook: playbook
      expect do
        delete play_url(play)
      end.to change(Play, :count).by(-1)
    end

    it 'redirects to the playbook list' do
      play = FactoryBot.create :play, playbook: playbook
      delete play_url(play)
      expect(response).to redirect_to(playbook_url(playbook))
    end
  end
end
