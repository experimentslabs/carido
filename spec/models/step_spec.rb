require 'rails_helper'

RSpec.describe Step do
  let(:playbook) { FactoryBot.create :playbook, :without_steps }

  describe 'hooks' do
    describe 'before_validation' do
      context 'with a new record' do
        it 'sets the position when unspecified' do
          step = described_class.new name: 'a step', playbook: playbook
          step.validate

          expect(step.position).to eq 0
        end

        it 'sets the step at the last position when unspecified' do
          FactoryBot.create :step, playbook: playbook
          step = described_class.new name: 'a step', playbook: playbook
          step.validate

          expect(step.position).to eq 1
        end

        it 'keeps provided position' do
          step = described_class.new name: 'a step', playbook: playbook, position: 10
          step.validate

          expect(step.position).to eq 10
        end
      end
    end

    describe 'after_create' do
      before do
        FactoryBot.create :step, name: 'original_0', playbook: playbook
        FactoryBot.create :step, name: 'original_1', playbook: playbook
      end

      context 'without a position' do
        it 'moves the step at the right position' do
          described_class.create! playbook: playbook, name: 'new step'
          list = playbook.steps.order(:position).pluck :position, :name

          expect(list).to eq [[0, 'original_0'], [1, 'original_1'], [2, 'new step']]
        end
      end

      context 'with a position' do
        it 'moves the step at the right position' do
          described_class.create! playbook: playbook, name: 'new step', position: 1
          list = playbook.steps.order(:position).pluck :position, :name

          expect(list).to eq [[0, 'original_0'], [1, 'new step'], [2, 'original_1']]
        end
      end
    end

    describe 'after_update' do
      let!(:steps) do
        [
          FactoryBot.create(:step, name: 'original_0', playbook: playbook),
          FactoryBot.create(:step, name: 'original_1', playbook: playbook),
          FactoryBot.create(:step, name: 'original_2', playbook: playbook),
        ]
      end
      let(:step) { steps.last }

      it 'moves the step at the right position' do
        step.update! position: 1
        list = playbook.steps.order(:position).pluck :position, :name

        expect(list).to eq [[0, 'original_0'], [1, 'original_2'], [2, 'original_1']]
      end
    end
  end

  describe '#move!', :doing do
    let!(:steps) do
      [
        FactoryBot.create(:step, name: 'original_0', playbook: playbook),
        FactoryBot.create(:step, name: 'original_1', playbook: playbook),
        FactoryBot.create(:step, name: 'original_2', playbook: playbook),
        FactoryBot.create(:step, name: 'original_3', playbook: playbook),
      ]
    end

    let(:step) { steps.second }

    it 'moves the item at the start of the list' do
      step.move! to: 0
      list = playbook.steps.order(:position).pluck :position, :name

      expect(list).to eq [[0, 'original_1'], [1, 'original_0'], [2, 'original_2'], [3, 'original_3']]
    end

    it 'moves the item in the middle of the list' do
      step.move! to: 2
      list = playbook.steps.order(:position).pluck :position, :name

      expect(list).to eq [[0, 'original_0'], [1, 'original_2'], [2, 'original_1'], [3, 'original_3']]
    end
  end
end
