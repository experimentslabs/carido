require 'rails_helper'

RSpec.describe PlayStepComment do
  let(:play) { FactoryBot.create :play }

  describe 'validation' do
    context 'with a non-existent step' do
      it 'is invalid' do
        comment = described_class.new play: play, step_id: 0
        comment.validate

        expect(comment.errors[:step]).to include 'does not exist in current play'
      end
    end
  end
end
