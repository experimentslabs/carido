require 'rails_helper'

RSpec.describe Play do
  let(:playbook) { FactoryBot.create :playbook }
  let(:play) { described_class.new title: 'The title', playbook: playbook }

  describe 'hooks' do
    describe 'before_create' do
      it 'fills the playbook data' do # rubocop:disable RSpec/ExampleLength
        play.save!

        aggregate_failures do
          expect(play.playbook_data).to be_a Hash
          expect(play.steps_data).to be_a Array
          expect(play.current_step_id).to be_present
          expect(play).to be_running
          expect(play.started_at).to be_present
        end
      end
    end
  end

  describe 'validation' do
    describe '#validate_steps_presence' do
      context 'when playbook has no step' do
        let(:playbook) { FactoryBot.create :playbook, steps: [] }

        it 'adds an error' do
          play.validate
          expect(play.errors[:playbook]).to include 'has no steps'
        end
      end
    end

    describe '#validate_current_step' do
      context 'when current step does not exist' do
        it 'adds an error' do
          play.save
          play.current_step_id = 0
          play.validate

          expect(play.errors[:current_step]).to include 'does not exist in steps list'
        end
      end
    end
  end

  describe '#considered_done?' do
    it 'considers "running" as not done' do
      play.status = described_class.statuses[:running]
      expect(play.considered_done?).to be false
    end

    it 'considers all the other statuses as done' do # rubocop:disable Spec/ExampleLength
      aggregate_failures do
        (described_class.statuses.keys - ['running']).each do |status|
          play.status = status
          expect(play.considered_done?).to be true
        end
      end
    end
  end

  describe '#next!' do
    before do
      play.save!
    end

    it 'switches to the next step' do
      play.next!

      expect(play.current_step_id).to eq play.steps_data.second['id']
    end

    it 'updates the step status' do
      play.next!

      expect(play.steps_data.first['status']).to eq 'done'
    end

    context 'with a custom status' do
      it 'uses the custom status' do
        play.next! status: 'skipped'

        expect(play.steps_data.first['status']).to eq 'skipped'
      end
    end

    context 'when step is the last step' do
      it 'finishes the play' do
        play.steps_data.count.times { play.next! }

        expect(play).to be_done
      end
    end
  end

  describe '#finish!' do
    before do
      play.save!
    end

    it 'updates the record' do
      play.finish!

      expect(play).to be_done
    end

    it 'handles a custom status' do
      play.finish! status: described_class.statuses['failed']

      expect(play).to be_failed
    end

    context 'when there are steps remaining' do
      it 'marks them all as "skipped"' do
        play.finish!
        statuses = play.steps_data.pluck('status').uniq

        expect(statuses).to eq ['skipped']
      end
    end
  end
end
