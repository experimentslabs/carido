require 'rails_helper'
require 'pundit/rspec'

RSpec.describe PlayPolicy, type: :policy do
  subject { described_class }

  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:playbook) { FactoryBot.create :playbook, user: user }
  let(:play) { FactoryBot.build :step, playbook: playbook }

  permissions '.scope' do
    it 'filters plays by user' do
      FactoryBot.create :play, playbook: playbook
      another_playbook = FactoryBot.create :playbook, user: other_user
      FactoryBot.create_list :play, 2, playbook: another_playbook

      expect(Pundit.policy_scope(user, Play).count).to eq 1
    end
  end

  permissions :index? do
    it 'allows access' do
      expect(described_class).to permit(user, Play)
    end
  end

  permissions :create? do
    it 'allows access' do
      expect(described_class).to permit(user, Play)
    end
  end

  permissions :show?, :update?, :destroy?, :next_step?, :finish? do
    it 'allows access' do
      expect(described_class).to permit(user, play)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, play)
    end
  end
end
