require 'rails_helper'
require 'pundit/rspec'

RSpec.describe StepPolicy, type: :policy do
  subject { described_class }

  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:playbook) { FactoryBot.build :playbook, :without_steps, user: user }
  let(:step) { FactoryBot.build :step, playbook: playbook }

  permissions '.scope' do
    it 'filters steps by user' do
      FactoryBot.create :step, playbook: playbook
      another_playbook = FactoryBot.create :playbook, user: other_user
      FactoryBot.create_list :step, 2, playbook: another_playbook

      expect(Pundit.policy_scope(user, Step).count).to eq 1
    end
  end

  permissions :index? do
    it 'allows access' do
      expect(described_class).to permit(user, Step)
    end
  end

  permissions :show? do
    it 'allows access' do
      expect(described_class).to permit(user, step)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, step)
    end
  end

  permissions :create? do
    it 'allows access' do
      expect(described_class).to permit(user, Step)
    end
  end

  permissions :update? do
    it 'allows access' do
      expect(described_class).to permit(user, step)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, step)
    end
  end

  permissions :destroy? do
    it 'allows owner' do
      expect(described_class).to permit(user, step)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, step)
    end
  end
end
