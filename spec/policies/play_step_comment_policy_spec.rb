require 'rails_helper'
require 'pundit/rspec'

RSpec.describe PlayStepCommentPolicy, type: :policy do
  subject { described_class }

  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:play) { FactoryBot.create :play, playbook: FactoryBot.create(:playbook, user: user) }
  let(:other_play) { FactoryBot.create :play, playbook: FactoryBot.create(:playbook, user: other_user) }

  let(:play_step_comment) { FactoryBot.create :play_step_comment, play: play, step_id: play.steps_data.first['id'] }

  permissions '.scope' do
    it 'filters comments by user' do
      FactoryBot.create :play_step_comment, play: play, step_id: play.steps_data.first['id']
      FactoryBot.create_list :play_step_comment, 2, play: other_play, step_id: other_play.steps_data.first['id']

      expect(Pundit.policy_scope(user, PlayStepComment).count).to eq 1
    end
  end

  permissions :index? do
    it 'allows access' do
      expect(described_class).to permit(user, PlayStepComment)
    end
  end

  permissions :create? do
    it 'allows access' do
      expect(described_class).to permit(user, PlayStepComment)
    end
  end

  permissions :show?, :update?, :destroy? do
    it 'allows access' do
      expect(described_class).to permit(user, play_step_comment)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, play_step_comment)
    end
  end
end
