require 'rails_helper'
require 'pundit/rspec'

RSpec.describe PlaybookPolicy, type: :policy do
  subject { described_class }

  let(:user) { FactoryBot.create :user }
  let(:other_user) { FactoryBot.create :user }
  let(:playbook) { FactoryBot.build :playbook, user: user }

  permissions '.scope' do
    it 'filters playbooks by user' do
      FactoryBot.create :playbook, user: user
      FactoryBot.create_list :playbook, 2, user: other_user

      expect(Pundit.policy_scope(user, Playbook).count).to eq 1
    end
  end

  permissions :index? do
    it 'allows access' do
      expect(described_class).to permit(user, Playbook)
    end
  end

  permissions :show? do
    it 'allows access' do
      expect(described_class).to permit(user, playbook)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, playbook)
    end
  end

  permissions :new? do
    it 'allows access' do
      expect(described_class).to permit(user, Playbook)
    end
  end

  permissions :create? do
    it 'allows access' do
      expect(described_class).to permit(user, Playbook)
    end
  end

  permissions :edit? do
    it 'allows access' do
      expect(described_class).to permit(user, playbook)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, playbook)
    end
  end

  permissions :update? do
    it 'allows access' do
      expect(described_class).to permit(user, playbook)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, playbook)
    end
  end

  permissions :destroy? do
    it 'allows owner' do
      expect(described_class).to permit(user, playbook)
    end

    it 'denies access to other users' do
      expect(described_class).not_to permit(other_user, playbook)
    end
  end
end
