RSpec.shared_context 'with an authenticated user' do
  let(:user) { FactoryBot.create :user }

  before do
    sign_in user
  end
end
