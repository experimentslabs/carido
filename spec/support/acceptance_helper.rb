require 'rails_helper'
require 'rspec_rails_api'

RSpec.configure do |config|
  config.include RSpec::Rails::Api::DSL::Example
end

renderer = RSpec::Rails::Api::OpenApiRenderer.new
# Options here should be customized
renderer.api_title       = 'carido'
renderer.api_version     = '1'
renderer.api_description = 'A wonderful Rails application'
# Options below are optional
# renderer.api_servers = [{ url: 'https://production.url' }]

RSpec.configuration.after(:context, type: :acceptance) do |context|
  renderer.merge_context context.class.metadata[:rra].to_h
end
