FactoryBot.define do
  factory :play do
    title { Faker::Lorem.sentence }

    playbook
  end
end
