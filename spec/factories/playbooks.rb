FactoryBot.define do
  factory :playbook do
    name { Faker::Lorem.sentence.tr '.', '' }
    steps { FactoryBot.build_list :step, 2, playbook_id: nil }

    user

    trait :without_steps do
      steps { [] }
    end
  end
end
