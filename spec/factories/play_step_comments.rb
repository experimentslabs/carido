FactoryBot.define do
  factory :play_step_comment do
    content { Faker::Markdown.sandwich }
    play
    step_id { play.steps_data.first['id'] }
  end
end
