FactoryBot.define do
  factory :step do
    name { Faker::Lorem.word }

    playbook
  end
end
