Rails.application.routes.draw do
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get 'up' => 'rails/health#show', as: :rails_health_check

  # Defines the root path route ("/")
  root 'playbooks#index'

  resources :playbooks, except: [:index] do
    resources :steps, except: [:destroy, :update]
    resources :plays, except: [:destroy, :update]
  end
  resources :steps, only: [:destroy, :update]
  resources :plays, only: [:destroy, :update] do
    member do
      put :next_step
      put :finish
    end
    resources :play_step_comments, except: [:show, :destroy, :update]
  end
  resources :play_step_comments, only: [:destroy, :update]
end
