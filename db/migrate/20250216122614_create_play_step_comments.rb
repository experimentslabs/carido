class CreatePlayStepComments < ActiveRecord::Migration[7.1]
  def change
    create_table :play_step_comments do |t|
      t.references :play, null: false, foreign_key: true
      t.integer :step_id, null: false, default: nil, comment: 'Not a foreign key, but a reference on step in play\'s "steps_data"'
      t.text :content, null: false, default: nil

      t.timestamps
    end
  end
end
