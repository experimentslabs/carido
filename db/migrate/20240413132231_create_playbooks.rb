class CreatePlaybooks < ActiveRecord::Migration[7.1]
  def change
    create_table :playbooks do |t|
      t.string :name, null: false, default: nil
      t.text :description
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
