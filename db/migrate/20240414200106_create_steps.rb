class CreateSteps < ActiveRecord::Migration[7.1]
  def change
    create_table :steps do |t|
      t.string :name
      t.text :content
      t.integer :position, null: false, default: 0

      t.references :playbook, null: false, foreign_key: true

      t.timestamps
    end
  end
end
