class CreatePlays < ActiveRecord::Migration[7.1]
  def change
    create_table :plays do |t|
      t.string :title, null: false, default: nil
      t.text :description
      t.references :playbook, null: false, foreign_key: true
      t.jsonb :playbook_data, null: false, default: nil, comment: 'Playbook information at the moment the play started'
      t.jsonb :steps_data, null: false, default: nil, comment: 'List of steps at the moment the play started'
      t.integer :current_step_id, null: false, default: nil, comment: 'Not a foreign key; current step id in the list of "steps_data"'
      t.integer :status, null: false, default: nil

      t.datetime :started_at, null: false, default: nil
      t.datetime :ended_at

      t.timestamps
    end
  end
end
