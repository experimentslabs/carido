# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2025_02_16_122614) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "play_step_comments", force: :cascade do |t|
    t.bigint "play_id", null: false
    t.integer "step_id", null: false, comment: "Not a foreign key, but a reference on step in play's \"steps_data\""
    t.text "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["play_id"], name: "index_play_step_comments_on_play_id"
  end

  create_table "playbooks", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_playbooks_on_user_id"
  end

  create_table "plays", force: :cascade do |t|
    t.string "title", null: false
    t.text "description"
    t.bigint "playbook_id", null: false
    t.jsonb "playbook_data", null: false, comment: "Playbook information at the moment the play started"
    t.jsonb "steps_data", null: false, comment: "List of steps at the moment the play started"
    t.integer "current_step_id", null: false, comment: "Not a foreign key; current step id in the list of \"steps_data\""
    t.integer "status", null: false
    t.datetime "started_at", null: false
    t.datetime "ended_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["playbook_id"], name: "index_plays_on_playbook_id"
  end

  create_table "steps", force: :cascade do |t|
    t.string "name"
    t.text "content"
    t.integer "position", default: 0, null: false
    t.bigint "playbook_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["playbook_id"], name: "index_steps_on_playbook_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "play_step_comments", "plays"
  add_foreign_key "playbooks", "users"
  add_foreign_key "plays", "playbooks"
  add_foreign_key "steps", "playbooks"
end
