import { Application } from '@hotwired/stimulus'
import HelloController from './hello_controller.js'
import { describe, it, expect, beforeEach } from 'vitest'

describe('HelloController', () => {
  let application

  beforeEach(() => {
    document.body.innerHTML = `
      <div data-controller="hello" id="test-element">
        Initial content
      </div>
    `

    application = Application.start()
    application.register('hello', HelloController)
  })

  it('replaces text content', () => {
    expect.assertions(1)
    expect(document.getElementById('test-element').innerHTML).toBe('Hello World!')
  })
})
