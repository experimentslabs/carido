// import ActiveStorage from '@rails/activestorage'
import { Turbo } from '@hotwired/turbo-rails'
// const channels = import.meta.globEager('./**/*_channel.js')

import '../stylesheets/style.scss'

// Stimulus controllers
import '../controllers'

// Rails ActiveStorage
// ActiveStorage.start()

Turbo.start()
