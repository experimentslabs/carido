class PlayPolicy < MemberPolicy
  class Scope < Scope
    def resolve
      Play.joins(:playbook).where(playbooks: { user_id: @user.id })
    end
  end

  def next_step?
    update?
  end

  def finish?
    update?
  end

  private

  def owner?
    @user.id == @record.playbook.user_id
  end
end
