class StepPolicy < MemberPolicy
  class Scope < Scope
    def resolve
      Step.joins(:playbook).where(playbooks: { user_id: @user.id })
    end
  end

  private

  def owner?
    @user.id == @record.playbook.user_id
  end
end
