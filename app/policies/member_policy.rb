class MemberPolicy < ApplicationPolicy
  def index?
    signed_in?
  end

  def show?
    owner?
  end

  def create?
    signed_in?
  end

  def new?
    create?
  end

  def update?
    owner?
  end

  def edit?
    update?
  end

  def destroy?
    owner?
  end

  private

  def signed_in?
    @user.present?
  end

  def owner?
    @user.id == @record.user_id
  end
end
