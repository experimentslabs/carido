class PlayStepCommentPolicy < MemberPolicy
  class Scope < Scope
    def resolve
      PlayStepComment.joins(play: [:playbook]).where(playbook: { user_id: @user.id })
    end
  end

  private

  def owner?
    @user.id == @record.play.playbook.user_id
  end
end
