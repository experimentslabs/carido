class PlaybookPolicy < MemberPolicy
  class Scope < Scope
    def resolve
      @user.playbooks
    end
  end
end
