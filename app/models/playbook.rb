class Playbook < ApplicationRecord
  validates :name, presence: true, allow_blank: false

  belongs_to :user

  has_many :steps, dependent: :destroy
  has_many :plays, dependent: :destroy
end
