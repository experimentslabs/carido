class PlayStepComment < ApplicationRecord
  validates :content, presence: true, allow_blank: false
  validates :step_id, presence: true, allow_blank: false
  validate :validate_step_presence

  belongs_to :play

  private

  def validate_step_presence
    return if step_id.blank?

    errors.add :step, 'does not exist in current play' unless play.contains_step? step_id
  end
end
