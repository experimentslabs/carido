class Play < ApplicationRecord
  STEP_STATUSES = %w[skipped failed done].freeze

  validates :title, presence: true, allow_blank: false
  validate :validate_steps_presence, on: :create
  validate :validate_current_step, on: :update

  enum :status, {
    running:   1,
    abandoned: 2,
    done:      3,
    failed:    4,
  }

  belongs_to :playbook
  has_many :play_step_comments, dependent: :destroy

  before_create :fill_metadata

  def next!(status: nil)
    prevent_changes!

    index = steps_data.find_index { |d| d['id'] == current_step_id }
    raise ActiveRecord::RecordNotFound, "No step found in current play with id #{current_step_id}" unless index

    next_or_finish! current_step_index: index, current_step_status: status
  end

  def finish!(status: nil)
    prevent_changes!

    skip_remaining
    update! status:   status || Play.statuses[:done],
            ended_at: Time.zone.now
  end

  def considered_done?
    !running?
  end

  def contains_step?(id)
    steps_data.find_index { |d| d['id'] == id }.present?
  end

  private

  def next_or_finish!(current_step_index:, current_step_status: nil)
    steps_data[current_step_index]['status'] = current_step_status.presence || 'done'

    next_step = steps_data[current_step_index + 1]
    if next_step
      self.current_step_id = next_step['id']
      save!
    elsif current_step_index + 1 == steps_data.count
      finish!
    end
  end

  def skip_remaining
    index = steps_data.find_index { |d| d['id'] == current_step_id }

    loop do
      step = steps_data[index]
      break unless step

      step['status'] = 'skipped' if step
      index += 1
    end
  end

  def prevent_changes!
    raise 'Play is considered done and cannot be changed' if considered_done?
  end

  def validate_steps_presence
    errors.add :playbook, 'has no steps' unless playbook&.steps&.count&.positive?
  end

  def validate_current_step
    errors.add :current_step, 'does not exist in steps list' unless steps_data.find_index { |d| d['id'] == current_step_id }
  end

  def fill_metadata # rubocop:disable Metrics/AbcSize
    raise 'Cannot fill metadata on existing plays' if persisted?

    steps_list = playbook.steps.order(position: :asc)

    self.playbook_data   = { name: playbook.name, description: playbook.description, version: playbook.updated_at }
    self.steps_data      = steps_list.map { |step| { id: step.id, name: step.name, content: step.content, position: step.position, version: step.updated_at, status: '' } }
    self.current_step_id = steps_list.first.id
    self.status          = Play.statuses[:running]
    self.started_at      = Time.zone.now
  end
end
