class Step < ApplicationRecord
  validates :name, presence: true, allow_blank: false

  belongs_to :playbook

  before_validation :initialize_position, unless: :persisted?
  after_create :apply_position
  after_update :apply_position, if: :position_previously_changed?

  # Updates the other steps position
  #
  # @param to [Integer] Where to place the step. Step with same position will be placed _after_ this step.
  # @param in_hook [Boolean] Whether to also update the step's position. This is only here to ease executions
  #                          in hooks, when the new position is already saved, and should be left as-is in most
  #                          of the cases
  def move!(to:, in_hook: false) # rubocop:disable Metrics/AbcSize
    raise 'Cannot move without a playbook' unless playbook

    # When in hooks (create/update), final position is already set, so we do as if the step was moved from the end of the list
    from = in_hook && position == to ? playbook.steps.count + 1 : position
    selector = from < to ? 'position > ? AND position <= ?' : 'position < ? AND position >= ?'
    change   = from < to ? 'position = position - 1' : 'position = position + 1'

    transaction do
      playbook.steps.where.not(id: id).where(selector, from, to).update_all change # rubocop:disable Rails/SkipsModelValidations
      update_column :position, to unless in_hook # rubocop:disable Rails/SkipsModelValidations
    end
  end

  def self.fix_positions!
    Playbook.find_each do |playbook|
      transaction do
        playbook.steps.order(:position).each_with_index do |step, i|
          step.update_column :position, i # rubocop:disable Rails/SkipsModelValidations
        end
      end
    end
  end

  private

  # Changes position on other steps without updating self position
  def apply_position
    move! to: position, in_hook: true
  end

  # Determines initial position in the playbook
  def initialize_position
    return if position_came_from_user?

    last_step = playbook.steps.order(:position).last
    self.position = last_step ? last_step.position + 1 : 0
  end
end
