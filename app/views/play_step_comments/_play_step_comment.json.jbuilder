json.extract! play_step_comment, :id, :play_id, :step_id, :content, :created_at, :updated_at
json.url play_step_comment_url(play_step_comment, format: :json)
