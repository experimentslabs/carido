json.extract! playbook, :id, :name, :description, :user_id, :created_at, :updated_at
json.url playbook_url(playbook, format: :json)
