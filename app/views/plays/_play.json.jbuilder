json.extract! play, :id, :title, :description, :playbook_id, :playbook_data, :status, :created_at, :updated_at
json.url play_url(play, format: :json)
