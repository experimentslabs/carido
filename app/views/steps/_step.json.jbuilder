json.extract! step, :id, :playbook_id, :name, :content, :position, :created_at, :updated_at
json.url step_url(step, format: :json)
