class ApplicationController < ActionController::Base
  include Pundit::Authorization

  rescue_from ActionController::InvalidAuthenticityToken, with: :error_csrf
  rescue_from ActionController::ParameterMissing, with: :error_unprocessable
  rescue_from ActiveRecord::RecordNotFound, with: :error_not_found
  rescue_from Pundit::NotAuthorizedError, with: :error_not_authorized

  private

  def render_error(exception, fallback_message, status)
    message = exception&.message || fallback_message
    respond_to do |format|
      format.json { render json: { error: message }, status: status }
      format.html { raise exception }
    end
  end

  def error_not_found(exception = nil)
    render_error(exception, I18n.t('controller.application.error_not_found'), :not_found)
  end

  def error_unprocessable(exception = nil)
    render_error(exception, I18n.t('controller.application.error_unprocessable_entity'), :unprocessable_entity)
  end

  def error_csrf(exception = nil)
    render_error(exception, I18n.t('controller.application.error_invalid_csrf_token'), :unprocessable_entity)
  end

  def error_not_authorized(_exception = nil)
    message = I18n.t('controller.application.error_not_authorized')
    respond_to do |format|
      format.json { render json: { error: message }, status: :unauthorized }
      format.html { redirect_to new_user_session_path }
    end
  end
end
