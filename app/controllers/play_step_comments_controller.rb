class PlayStepCommentsController < ApplicationController
  before_action :set_play, only: [:index, :create]
  before_action :set_play_step_comment, only: [:edit, :update, :destroy]

  # GET /play_step_comments or /play_step_comments.json
  def index
    @play_step_comments = policy_scope(PlayStepComment).where(play: @play)
  end

  # GET /play_step_comments/new
  def new
    @play_step_comment = PlayStepComment.new play_id: params[:play_id]
  end

  # GET /play_step_comments/1/edit
  def edit; end

  # POST /play_step_comments or /play_step_comments.json
  def create # rubocop:disable Metrics/AbcSize
    @play_step_comment = PlayStepComment.new(play_step_comment_params)
    @play_step_comment.play = @play

    respond_to do |format|
      if @play_step_comment.save
        format.html { redirect_to playbook_play_url(@play.playbook, @play), notice: I18n.t('controller.play_step_comment.create.success') }
        format.json { render :show, status: :created, location: @play_step_comment }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @play_step_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /play_step_comments/1 or /play_step_comments/1.json
  def update # rubocop:disable Metrics/AbcSize
    respond_to do |format|
      if @play_step_comment.update(play_step_comment_params)
        format.html { redirect_to playbook_play_url(@play_step_comment.play.playbook_id, @play_step_comment.play), notice: I18n.t('controller.play_step_comment.update.success') }
        format.json { render :show, status: :ok, location: @play_step_comment }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @play_step_comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /play_step_comments/1 or /play_step_comments/1.json
  def destroy
    @play_step_comment.destroy!

    respond_to do |format|
      format.html { redirect_to playbook_play_url(@play_step_comment.play.playbook, @play_step_comment.play), notice: I18n.t('controller.play_step_comment.destroy.success') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_play_step_comment
    @play_step_comment = PlayStepComment.find(params[:id])

    authorize @play_step_comment
  end

  def set_play
    @play = Play.find(params[:play_id])

    authorize @play
  end

  # Only allow a list of trusted parameters through.
  def play_step_comment_params
    params.require(:play_step_comment).permit(:step_id, :content)
  end
end
