class PlaybooksController < ProtectedController
  before_action :set_playbook, only: [:show, :edit, :update, :destroy]

  # GET /playbooks or /playbooks.json
  def index
    @playbooks = policy_scope(Playbook)
  end

  # GET /playbooks/1 or /playbooks/1.json
  def show; end

  # GET /playbooks/new
  def new
    @playbook = Playbook.new
  end

  # GET /playbooks/1/edit
  def edit; end

  # POST /playbooks or /playbooks.json
  def create # rubocop:disable Metrics/AbcSize
    @playbook = Playbook.new(playbook_params)
    @playbook.user = current_user

    respond_to do |format|
      if @playbook.save
        format.html { redirect_to playbook_url(@playbook), notice: I18n.t('controller.playbooks.create.success') }
        format.json { render :show, status: :created, location: @playbook }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @playbook.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /playbooks/1 or /playbooks/1.json
  def update
    respond_to do |format|
      if @playbook.update(playbook_params)
        format.html { redirect_to playbook_url(@playbook), notice: I18n.t('controller.playbooks.update.success') }
        format.json { render :show, status: :ok, location: @playbook }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @playbook.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /playbooks/1 or /playbooks/1.json
  def destroy
    @playbook.destroy!

    respond_to do |format|
      format.html { redirect_to root_url, notice: I18n.t('controller.playbooks.destroy.success') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_playbook
    @playbook = Playbook.find(params[:id])

    authorize @playbook
  end

  # Only allow a list of trusted parameters through.
  def playbook_params
    params.require(:playbook).permit(:name, :description)
  end
end
