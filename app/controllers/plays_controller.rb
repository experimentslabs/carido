class PlaysController < ApplicationController
  before_action :set_playbook, only: [:index, :show, :create]
  before_action :set_play, only: [:show, :edit, :update, :destroy, :next_step, :finish]

  # GET /plays or /plays.json
  def index
    @plays = policy_scope(Play).where(playbook: @playbook)
  end

  # GET /plays/1 or /plays/1.json
  def show; end

  # GET /plays/new
  def new
    @play = Play.new
  end

  # GET /plays/1/edit
  def edit; end

  # POST /plays or /plays.json
  def create # rubocop:disable Metrics/AbcSize
    @play = Play.new(play_params)
    @play.playbook = @playbook

    respond_to do |format|
      if @play.save
        format.html { redirect_to playbook_play_url(@play.playbook, @play), notice: I18n.t('controller.play.create.success') }
        format.json { render :show, status: :created, location: @play }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @play.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plays/1 or /plays/1.json
  def update
    respond_to do |format|
      if @play.update(play_params)
        format.html { redirect_to playbook_play_url(@play.playbook, @play), notice: I18n.t('controller.play.update.success') }
        format.json { render :show, status: :ok, location: @play }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @play.errors, status: :unprocessable_entity }
      end
    end
  end

  def next_step
    respond_to do |format|
      if @play.next! status: params[:status]
        format.html { redirect_to playbook_play_url(@play.playbook, @play), notice: I18n.t('controller.play.update.success') }
        format.json { render :show, status: :ok, location: @play }
      end
    end
  end

  def finish
    respond_to do |format|
      if @play.finish! status: params[:status]
        format.html { redirect_to playbook_play_url(@play.playbook, @play), notice: I18n.t('controller.play.update.success') }
        format.json { render :show, status: :ok, location: @play }
      end
    end
  end

  # DELETE /plays/1 or /plays/1.json
  def destroy
    @play.destroy!

    respond_to do |format|
      format.html { redirect_to playbook_url(@play.playbook), notice: I18n.t('controller.play.destroy.success') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_play
    @play = Play.find(params[:id])

    authorize @play
  end

  def set_playbook
    @playbook = Playbook.find(params[:playbook_id])

    authorize @playbook
  end

  # Only allow a list of trusted parameters through.
  def play_params
    params.require(:play).permit(:title, :description)
  end
end
