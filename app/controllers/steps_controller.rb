class StepsController < ProtectedController
  before_action :set_playbook, only: [:index, :show, :create]
  before_action :set_step, only: [:show, :edit, :update, :destroy]

  # GET /steps or /steps.json
  def index
    @steps = policy_scope(Step).where(playbook_id: params[:playbook_id])
  end

  # GET /steps/1 or /steps/1.json
  def show; end

  # GET /steps/new
  def new
    @step = Step.new
  end

  # GET /steps/1/edit
  def edit; end

  # POST /steps or /steps.json
  def create # rubocop:disable Metrics/AbcSize
    @step = Step.new(step_params)
    @step.playbook = @playbook

    respond_to do |format|
      if @step.save
        format.html { redirect_to playbook_url(@step.playbook), notice: I18n.t('controller.step.create.success') }
        format.json { render :show, status: :created, location: @step }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /steps/1 or /steps/1.json
  def update
    respond_to do |format|
      if @step.update(step_params)
        format.html { redirect_to playbook_url(@step.playbook), notice: I18n.t('controller.step.update.success') }
        format.json { render :show, status: :ok, location: @step }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /steps/1 or /steps/1.json
  def destroy
    @step.destroy!

    respond_to do |format|
      format.html { redirect_to playbook_url(@step.playbook), notice: I18n.t('controller.step.destroy.success') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_step
    @step = Step.find(params[:id])

    authorize @step
  end

  def set_playbook
    @playbook = Playbook.find(params[:playbook_id])

    authorize @playbook
  end

  # Only allow a list of trusted parameters through.
  def step_params
    params.require(:step).permit(:name, :content, :position)
  end
end
