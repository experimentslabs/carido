require 'i18n/model_attributes'

namespace :i18n do
  desc 'Generate ActiveRecord models and attributes list to help i18n-task'
  task 'add-models-attributes': :environment do
    I18n::ModelAttributes.generate
  end

  desc 'Add missing translations'
  task 'add-missing': :environment do
    system 'i18n-tasks add-missing -v "TRANSLATE_ME %{human_key}"' # rubocop:disable Style/FormatStringToken
  end
end
