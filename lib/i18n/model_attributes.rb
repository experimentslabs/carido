module I18n
  class ModelAttributes
    class << self
      IGNORED_MODELS = %w[].freeze

      I18N_FILE = File.join('app', 'i18n', 'model_attributes.i18n')

      def generate # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
        FileUtils.rm_f(I18N_FILE)
        @content = ''

        Dir.glob(File.join('app', 'models', '*.rb')).each do |f|
          class_name = File.basename(f, '.rb').camelize
          class_obj  = class_name.constantize

          next if IGNORED_MODELS.include?(class_obj.name) || class_obj.abstract_class

          model = class_name.split('::').join('.').underscore
          Rails.logger.debug { "Checking for model #{class_name} (#{model})" }
          add_i18n_tasks_use_for_activerecord_model(model)

          add_attributes model, class_obj
        end

        File.write(I18N_FILE, <<~TXT)
          # Generated using "rake i18n:add-models-attributes" - Do not modify manually

          #{@content}
        TXT
      end

      # @param model [String] - The underscored, singular model name
      # @param class_obj [ActiveRecord::Base] - The model class
      def add_attributes(model, class_obj)
        class_obj.attribute_types.sort.each do |attribute, type|
          next if attribute == 'id'

          # Standard attributes
          add_i18n_tasks_use_for_activerecord_attribute(model, attribute.sub(/_id$/, ''))

          next unless type.is_a? ActiveRecord::Enum::EnumType

          # Enums attributes
          class_obj.send(attribute.pluralize).keys.sort.each do |enum_option|
            add_i18n_tasks_use_for_activerecord_attribute("#{model}/#{attribute}", enum_option)
          end
        end
      end

      private

      def add_i18n_tasks_use(key)
        @content += "# i18n-tasks-use t('#{key}')\n"
      end

      def add_i18n_tasks_use_for_activerecord_model(model)
        add_i18n_tasks_use("activerecord.models.#{model}")
      end

      def add_i18n_tasks_use_for_activerecord_attribute(model, attribute)
        add_i18n_tasks_use("activerecord.attributes.#{model}.#{attribute}")
      end
    end
  end
end
