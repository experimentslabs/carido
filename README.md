# Readme

CariDo is an app to manage and _play_ repetitive tasks.

It is useful to document

- recipes
- procedures
- repetitive todo lists
- ...

The idea comes from [Mattermost's Playbooks](https://docs.mattermost.com/repeatable-processes/learn-about-playbooks.html),
but without Mattermost.


## Links:

- [CHANGELOG.md](CHANGELOG.md)
- [ROADMAP.md](ROADMAP.md) - Planned changes. May not be up-to-date. Some ideas may change/disappear before they are implemented.
- [LICENSE](LICENCE) - MIT

## Getting started

### Database configuration

We use PostgreSQL for this project.

To get started, copy `config/database.default.yml` to `config/database.yml`
and modify the file to match your setup.

```sh
cp config/database.default.yml config/database.yml
```

Create database and run the migrations and seeds:

```sh
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails db:seed
```

Drop the database:

```sh
bundle exec rails db:drop
```

## Development

### FactoryBot - [site](https://github.com/thoughtbot/factory_bot)

FactoryBot Rails allows to quickly create entities. It's useful in
tests, as creating fixtures can be a real pain.

They are located in `spec/factories`, and are available in Rails
console, RSpec and Cucumber. It's also used for development seeds.

Note that FactoryBot is not available in production.

FactoryBot is configured to create related entities by default, instead
of only building them (`FactoryBot.use_parent_strategy = false`)

#### Check the factories

A rake task can be used to test the ability to run a factory multiple
times:

```sh
rake factory_bot:lint
```

This task is executed in CI

### Faker - [site](https://github.com/faker-ruby/faker)

Faker is used during development to generate fake data. It should be
used in new factories.

### Authorization

Authorization is managed with
[Pundit](https://github.com/varvet/pundit).

Usage of `authorize xxx` and `policy_scope` are not enforced in
controllers, but in RSpec tests (check `spec/rails_helper.rb`).

### ViteJS

We use [Vite](https://vitejs.dev) for bundling and development server.

Configuration files are `config/vite.json` and `vite.config.js`

### Style

Stylesheets are located in `app/frontend/stylesheets/`. We use SCSS language.

### Internationalization

Internationalization is made as in "traditional" Rails applications, and
is managed with [i18n-tasks](https://github.com/glebm/i18n-tasks)

`i18n-tasks` helps to check if your translations are up-to-date.

A custom rake task exists to add missing translations, prefixed with
`TRANSLATE_ME` to find them easily, and another one adds the model
attributes in a dummy file, so they can be translated.

```sh
# Check
i18n-tasks health
# Add missing model attributes in `app/i18n/model_attributes.i18n`
rake i18n:add-models-attributes
# Add missing translations
rake i18n:add-missing
# Remove unused translations
i18n-tasks remove-unused
```

Check [config/i18n-tasks.yml](config/i18n-tasks.yml) for configuration.

An RSpec test checks for missing/unused translations, and a CI job tests
for missing model attributes translations.

### ERB views

To help having a consistent formatting, `erblint` checks files in CI.

To run it, simply execute:

```sh
bundle exec erblint app
```

### Seeds

There are 3 seeds files available in the project:

- `db/seeds_development.rb`
- `db/seeds_production.rb`
- `db/seeds.rb` for shared seeds.

When you seed the database with `rails db:seed`, shared seeds are run
first, then one of the other files is executed, depending on the
environment.

### Continuous integration

CI jobs are configured for Gitlab. Check
`[.gitlab-ci.yml](.gitlab-ci.yml)` to see the list.

## Testing

### Rubocop - [site](https://rubocop.org/)

Rubocop checks for coding standards, allowing us to have consistent
coding style in the project. Configuration is in
[.rubocop.yml](.rubocop.yml).

Enabled plugins:

- [rubocop-performance](https://docs.rubocop.org/projects/performance),
- [rubocop-rails](https://docs.rubocop.org/projects/rails/) for common
  errors in Rails projects
- [rubocop-rspec](https://github.com/rubocop-hq/rubocop-rspec)

Run it with:

```sh
bundle exec rubocop
# To fix automatically what can be fixed:
bundle exec rubocop -a
```

### RSpec - [site](https://github.com/rspec/rspec)

RSpec examples are in `spec/`. Run the suite with:

```sh
bundle exec rspec
```

To debug step by step:
```sh
# Run this once
bundle exec rspec
# Then run this to replay failed examples
bundle exec rspec --only-failures
```

### Cucumber - [site](https://github.com/cucumber/cucumber-rails)

Cucumber is configured with
[capybara-screenshot](http://github.com/mattheworiordan/capybara-screenshot),
which makes HTML and png screenshots of pages when a step fails. Both HTML
and images screenshots are saved in `tmp/capybara_screenshots`.

By default, Cucumber will use Firefox to run the tests, but this can be
changed with the `CAPYBARA_DRIVER` environment variable:

```sh
# Default with firefox
bundle exec cucumber
# Variants
CAPYBARA_DRIVER=firefox-headless bundle exec cucumber
CAPYBARA_DRIVER=chrome bundle exec cucumber
CAPYBARA_DRIVER=chrome-headless bundle exec cucumber
```

When using Chrome/Chromium, Cucumber steps will fail on Javascript
errors.

The project uses the
[webdrivers](https://github.com/titusfortner/webdrivers) gem, which
manage the browsers respective drivers.

### Code coverage

When using RSpec or Cucumber, code coverage summary is generated in
`coverage/`. Don't hesitate to open it and check by yourself.

### Brakeman - [site](https://brakemanscanner.org/)

Brakeman is a "security scanner" for common mistakes leading to security
issues.

It can be launched with:

```sh
bundle exec brakeman
```

### ESLint - [site](https://github.com/eslint/eslint)

ESLint is configured to use Standard configuration and some small
tweaks.

Run ESLint with:
```sh
yarn run lint:js
# To auto-fix files:
yarn run lint:js --fix
```

### StyleLint - [site](https://github.com/stylelint/stylelint)

Stylelint is configured to use the
[stylelint-scss](https://github.com/kristerkari/stylelint-scss) plugin;
the
[stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard)
and
[stylelint-config-recommended-scss](https://github.com/kristerkari/stylelint-config-recommended-scss)
configurations.

```sh
yarn lint:style
// Fix with
yarn lint:style --fix
```

**Note:** don't even try to auto-fix your files, auto-fixing ignores the
"disabling" directives in your files.
