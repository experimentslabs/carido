# Roadmap

Note that the elements in this list may change during development; it' a
general idea of what will be done next.
<!-- You may list the next milestones and the changes you plan for them,
     or use your favorite VCS platform to track them -->

**[1.0.0] - Working application with all the base features:**
- ~~Base Rails application with linters, testing frameworks,...~~
- Users
- Playbook management (Create, Read, Update, Delete)
- Playbook execution
- Comments on execution steps
- Executions management (Create, Read, Delete)

**[2.0.0] - API, teams, tags**
- Fully functional API for the above features
- JWT authentication
- OpenApi documentation
- Tags for playbooks
- Teams support:
  - User invitations
  - Tasks assignations

**Other ideas**
- Admin area?
- Webhooks?
- Public, duplicatable playbooks?
- Import/export?
- Command Line Interface client ?
- Public runs?
